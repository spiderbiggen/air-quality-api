FROM python:3.8.6-slim as builder
WORKDIR /api
RUN apt-get update -qq && apt-get install -y build-essential python3-dev default-libmysqlclient-dev

COPY ./Pipfile* ./
RUN pip install pipenv
RUN pipenv install --system --deploy


FROM python:3.8.6-slim
WORKDIR /api
RUN apt-get update && apt-get install -y default-libmysqlclient-dev
RUN apt-get clean autoclean
RUN apt-get autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/
COPY --from=builder /usr/local/lib/python3.8/site-packages/ /usr/local/lib/python3.8/site-packages/
COPY ./app /api/app
COPY ./migrations /api/migrations
COPY ./air_quality.py /api/air_quality.py
COPY ./config.py /api/config.py

ENV FLASK_APP=air_quality.py

EXPOSE 5000

# Start the main process.
CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0"]

