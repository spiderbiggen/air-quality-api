from app import create_app, db, models

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'User': models.User,
        'Setting': models.Setting,
        'Building': models.Building,
        'Floor': models.Floor,
        'DeviceOnFloor': models.DeviceOnFloor,
        'Device': models.Device,
        'SensorData': models.SensorData,
    }
