docker build . -t air-quality-api:latest
docker-compose run --rm air-quality-api python3 -m flask db upgrade
docker-compose up -d --remove-orphans

