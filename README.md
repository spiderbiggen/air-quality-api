```
           _         ____              _ _ _                    _____ _____ 
     /\   (_)       / __ \            | (_) |             /\   |  __ \_   _|
    /  \   _ _ __  | |  | |_   _  __ _| |_| |_ _   _     /  \  | |__) || |  
   / /\ \ | | '__| | |  | | | | |/ _` | | | __| | | |   / /\ \ |  ___/ | |  
  / ____ \| | |    | |__| | |_| | (_| | | | |_| |_| |  / ____ \| |    _| |_ 
 /_/    \_\_|_|     \___\_\\__,_|\__,_|_|_|\__|\__, | /_/    \_\_|   |_____|
                                                __/ |                       
                                               |___/
```

# Setup

## Pipenv

pipenv is used to ensure that managing the project dependencies is simple and straightforward.
See [pipenv.org](https://docs.pipenv.org/en/latest/) for more details. To use pipenv follow the following steps:

Make sure that pipenv is installed by running: `pip install --user pipenv`

Install the required dependencies by running `pipenv install --ignore-pipfile`, the flag `--ignore-pipfile` makes sure
that all dependencies are the same as when they were tested. This will also create a python virtualenv to ensure that
the application runs with the correct python version and dependencies. Adding a new dependency is done in much the same
way, for example: `pipenv install requests` will install and save the requests library to pipenv. To open a shell that
uses the virtual environment with the required libraries for this project run `pipenv shell`

## Database

This project uses MYSQL or MARIADB as its database make sure to install and configure a database instance. A native
library needs to be installed to connect with the database from Python. For information on what to install
see [mysqlclient-python](https://pypi.org/project/mysqlclient/). For windows, it is easiest to install
the [mysql connector for Python](https://dev.mysql.com/downloads/connector/python/).

### Configuration

Some environment variables should be set to configure the api to use certain features like the mysql database and mail
server. The following environment variables are available

| VARIABLE              | DEFAULT       | DESCRIPTION                                                       |
|-----------------------|---------------|-------------------------------------------------------------------|
| `DB_HOST`             | `127.0.0.1`   | The host for the database. Can include port like `127.0.0.1:3306` |
| `DB_PORT`             | `3306`        | The port on which the database can be accessed                    |
| `DB_SCHEMA`           | `air_quality` | The database schema that should hold the data                     |
| `DB_USER`             | `air_quality` | The username for accessing the database                           |
| `DB_PASSWORD`         |               | The password for the database                                     |
| `DB_URL`              |               | This variable can be used to set all previous variables in one go. Useful for certain cloud services (requires a properly formatted mysql connection url) |
| | | |
| `MAIL_SERVER`         |               | The host for the mail server.                                     |
| `MAIL_PORT`           | `25`          | The port for the mail server.                                     |
| `MAIL_USE_TLS`        | `False`       | If the mail server should be accessed over TLS, can't be used together with `MAIL_USE_SSL` |
| `MAIL_USE_SSL`        | `False`       | If the mail server should be accessed over SSL, can't be used together with `MAIL_USE_TLS` |
| `MAIL_USERNAME`       |               | The username for accessing the mail server                        |
| `MAIL_PASSWORD`       |               | The password for accessing the mail server                        |
| `MAIL_DEFAULT_SENDER` |               | The email address that 'sent' the email                           |
| | | |
| `MQTT_BROKER_URL`     |               | The host for the mqtt broker                                      |
| `MQTT_BROKER_PORT`    | `1883`        | The port for the mqtt broker                                      |
| `MQTT_USERNAME`       |               | The username for the mqtt broker                                  |
| `MQTT_PASSWORD`       |               | The password for the mqtt broker                                  |
| `MQTT_KEEPALIVE`      | `30`          | How long a connection to the mqtt broker stays active in seconds  |
| `MQTT_TLS_ENABLED`    | `False`       | If the mqtt broker should be accessed over TLS                    |
| `MQTT_TLS_CA_CERTS`   |               | A string path to the CA certificate files that are to be treated as trusted by this client. Required if TLS is enabled |

## Flask

To setup the database make sure that the database and api are properly configured, see
the [previous heading](#configuration) on how to set up the api. Then run the following commands to initialize the
database and make sure that the database schema is up to date:

- `pipenv run flask db upgrade`

To run the application use: `pipenv run flask run`. Optionally create a `.env` file to easily add environment variables.
See [.env.template](.env.template) for available environment variables and the default configuration.

# Migrations

To generate new migrations from changes made to the models run the following
command: `pipenv run flask db migrate -m "name"`, where `name` can be replaced with a description of the migration.

To make sure that the migration is applied to the database run `pipenv run flask db upgrade`.

