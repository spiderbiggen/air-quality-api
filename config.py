import os
from distutils.util import strtobool

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

DEFAULT_DATABASE = 'air_quality'
LOCALHOST = '127.0.0.1'
MYSQL_PORT = 3306


def get_env_int_or_default(key, default=None):
    return parse_int_or_default(os.environ.get(key) or default, default)


def parse_int_or_default(inp, default=None):
    if inp is None:
        return default
    try:
        return int(inp)
    except ValueError:
        return default


def get_env_bool_or_default(key, default=False):
    return parse_bool_or_default(os.environ.get(key) or default, default)


def parse_bool_or_default(inp, default=None):
    if isinstance(inp, bool):
        return inp
    if inp is None:
        return default
    try:
        return bool(strtobool(inp))
    except ValueError:
        return default


def get_database_url():
    database_url = os.environ.get('DB_URL')
    if database_url is not None:
        return database_url

    db_host = os.environ.get('DB_HOST') or LOCALHOST  # default to localhost
    host_parts = db_host.split(':')
    if len(host_parts) >= 2:
        db_host = host_parts[0]
        db_port = parse_int_or_default(host_parts[1])
    else:
        db_port = get_env_int_or_default('DB_PORT', MYSQL_PORT)  # default mysql port

    db_database = os.environ.get('DB_SCHEMA') or DEFAULT_DATABASE

    db_user = os.environ.get('DB_USER') or DEFAULT_DATABASE
    db_password = os.environ.get('DB_PASSWORD')
    if db_password is None or db_port is None:
        return None

    print(f'[DATABASE] Using database: {db_database} on {db_host}:{db_port}')
    return f'mysql://{db_user}:{db_password}@{db_host}:{db_port}/{db_database}'


class Config(object):
    SQLALCHEMY_DATABASE_URI = get_database_url() or exit('ERROR: DATABASE not correctly configured')
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # Maybe turn this on later for easier change tracking

    MAIL_SERVER = os.environ.get('MAIL_SERVER') or exit('ERROR: MAIL not correctly configured')
    MAIL_USE_TLS = get_env_bool_or_default('MAIL_USE_TLS')
    MAIL_USE_SSL = get_env_bool_or_default('MAIL_USE_SSL')
    MAIL_PORT = get_env_int_or_default('MAIL_PORT', 25)
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or exit('ERROR: MAIL not correctly configured')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or exit('ERROR: MAIL not correctly configured')
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')
    MAIL_DEBUG = False

    MQTT_BROKER_URL = os.environ.get('MQTT_BROKER_URL') or exit('ERROR: MQTT not correctly configured')
    MQTT_BROKER_PORT = get_env_int_or_default('MQTT_BROKER_PORT', 1883)
    MQTT_USERNAME = os.environ.get('MQTT_USERNAME')
    MQTT_PASSWORD = os.environ.get('MQTT_PASSWORD')
    MQTT_KEEPALIVE = get_env_int_or_default('MQTT_KEEPALIVE', 30)
    MQTT_TLS_ENABLED = get_env_bool_or_default('MQTT_TLS_ENABLED')
    MQTT_TLS_CA_CERTS = os.environ.get('MQTT_TLS_CA_CERTS')

    FRONTEND_URL = os.environ.get('FRONTEND_URL')

    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY') or exit('ERROR: JWT_SECRET_KEY not set')
    JWT_ACCESS_TOKEN_EXPIRES = get_env_int_or_default('JWT_EXPIRES', 3600)

    MAX_PER_PAGE = get_env_int_or_default('MAX_ITEMS_PER_PAGE', 2000)  # default to 2000 for now
    JSON_SORT_KEYS = False
