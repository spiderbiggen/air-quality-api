Dear user,

To reset your password click on the following link:

{{ frontend }}/reset_password?token={{ token }}

If you have not requested a password reset simply ignore this message.

Sincerely,

The Air Quality Monitor Team