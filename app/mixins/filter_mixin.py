from datetime import datetime, timezone
from typing import Set, Dict
from sqlalchemy import asc
from app import db
from app.util import parse_query_filter, parse_query_order_by


class FilterMixin:

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        r"""
        Specifies which attributes can be queried. Attributes that end with _at will automatically get two more allowed
        attributes ending in _before and _after, which allows datetime attributes to be more easily queried.

        :return: set of attributes that can be used to filter this Model
        """
        return {'created_at', 'updated_at'}

    @classmethod
    def filtered_query(cls, query_args: bytes, query: db.Query = None, filter_deny: Set[str] = None,
                       order_deny: Set[str] = None, **kwargs) -> (db.Query, Dict[str, any], Dict[str, any]):
        r"""
        Create a query to filter this model based on arguments found in the query parameters of a request.
        For example b'filters[created_at]=2020-12-10T12:00:00+00:00&filters[updated_at]=2020-12-10T12:00:00+00:00'
        The query can be sorted using for example b'sort[created_at]=asc'. Order can sort on the same attributes as
        defined in :func:`__get_filter_keywords__` the sort order can be given as asc or desc and will default to asc if
        the value is neither of those but not empty.


        :param query: base query object
        :param query_args: raw query from request
        :param filter_deny: set of additional filter attributes that cannot be used for this filter
        :param order_deny: set of additional filter attributes that cannot be used for this order_by
        :return: an SQLAlchemy query that has filters applied from the request and is ordered by sort statements in the
                 query parameters.
        """
        query = query or cls.query
        query, filter_by = cls.__add_filters(query, query_args, filter_deny)
        query, order_by = cls.__add_order_by(query, query_args, order_deny)
        return query, filter_by, order_by

    @classmethod
    def __add_filters(cls, query: db.Query, query_args: bytes, filter_deny: Set[str] = None) \
            -> (db.Query, Dict[str, any]):
        r"""
        Parses query string for filters and removes filters for attributes that are not allowed or do not exist.
        :param query: an SQLAlchemy query
        :param query_args: the raw query string as bytes
        :param filter_deny: additional attributes that are not allowed
        :return: an SQLAlchemy query that has filters applied from the request
        """
        filter_keywords = cls.__get_filter_keywords__()
        filter_keywords = filter_keywords.union([
            item
            for keyword in filter_keywords if keyword.endswith('_at')
            for item in [
                keyword.replace('_at', '_before'),
                keyword.replace('_at', '_after')
            ]
        ])
        filter_keywords = filter_keywords - (filter_deny or set())
        filters = parse_query_filter(query_args)
        [filters.pop(key) for key in list(filters.keys() - filter_keywords)]
        return cls.__filter__(query, **filters), filters

    @classmethod
    def __add_order_by(cls, query: db.Query, query_args: bytes, order_deny: Set[str] = None) \
            -> (db.Query, Dict[str, any]):
        r"""
        Parses query string for sort statements and removes these for attributes that are not allowed or do not exist.
        Sorts by created_at ascending if no sort orders were found in the query.
        :param query: an SQLAlchemy query
        :param query_args: the raw query string as bytes
        :param order_deny: additional attributes that are not allowed
        :return: an SQLAlchemy query that is sorted according to query parameters from the request.
        """
        order_keywords = cls.__get_filter_keywords__() - (order_deny or set())
        order_by = parse_query_order_by(query_args)
        [order_by.pop(key) for key in list(order_by.keys() - order_keywords)]
        if len(order_by) == 0:
            query.order_by(asc('created_at'))
        else:
            for (_, order) in order_by.items():
                query = query.order_by(order)
        return query, order_by

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        r"""
        Applies the filters that were parsed from the query string. Extend this method to add special handling.

        :param query: The base SQLAlchemy query
        :param kwargs: dictionary of attributes and values that should be used to filter the query.
        :return: filtered SQLAlchemy query
        """
        for key, value in kwargs.items():
            if key.endswith('_before'):
                if isinstance(value, datetime):
                    query = query.filter(getattr(cls, key.replace('_before', '_at')) < value)
            elif key.endswith('_after'):
                if isinstance(value, datetime):
                    query = query.filter(getattr(cls, key.replace('_after', '_at')) > value)
            else:
                query = query.filter(getattr(cls, key) == value)
        return query
