import uuid


class UuidMixin:
    @classmethod
    def generate_uuid(cls):
        temp_uuid = str(uuid.uuid4())
        temp_model = cls.query.filter_by(uuid=temp_uuid).first()
        while temp_model is not None:
            temp_uuid = str(uuid.uuid4())
            temp_model = cls.query.filter_by(uuid=temp_uuid).first()
        return temp_uuid
