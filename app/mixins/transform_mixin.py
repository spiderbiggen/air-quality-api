from abc import abstractmethod
from typing import Any, Dict


class TransformMixin:
    @abstractmethod
    def to_dict(self, **kwargs):
        pass

    @abstractmethod
    def from_dict(self, data, **kwargs):
        pass

    @classmethod
    def validate_invalid_dict_values(cls, data: Dict[str, Any], **kwargs):
        invalid = []
        for key, value in data.items():
            if isinstance(value, str):
                if len(value) == 0 or value.isspace():
                    invalid.append(key)
        for key in invalid:
            del data[key]
