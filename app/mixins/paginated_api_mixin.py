from flask import url_for, current_app

from app.mixins import FilterMixin
from app.util import query_for


class PaginatedAPIMixin(object):
    @classmethod
    def to_collection_dict(cls, request, endpoint, query=None, **kwargs):
        query = query or cls.query
        page = request.args.get('page', 1, type=int)
        limit = request.args.get('limit', 10, type=int)
        includes = request.args.getlist('with[]')
        filter_by = order_by = None
        if issubclass(cls, FilterMixin):
            query, filter_by, order_by = cls.filtered_query(request.query_string, query=query, **kwargs)
        max_limit = current_app.config['MAX_PER_PAGE']
        resources = query.paginate(
            page=page, per_page=limit, max_per_page=max_limit, error_out=False
        )
        page = resources.page
        limit = resources.per_page
        items_ = [item.to_dict(includes=includes) for item in resources.items]
        if includes:
            kwargs['with[]'] = includes
        query_string = query_for(filter_by, order_by)
        kwargs['filter_deny'] = None
        kwargs['order_deny'] = None
        return {
            'items': items_,
            '_meta': {
                'page': page,
                'limit': limit,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, limit=limit, **query_string, **kwargs),
                'next': url_for(endpoint, page=page + 1, limit=limit, **query_string,
                                **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, limit=limit, **query_string,
                                **kwargs) if resources.has_prev else None
            }
        }
