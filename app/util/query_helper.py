import collections
from datetime import datetime, timezone
from typing import Dict, Any, OrderedDict
from urllib.parse import parse_qsl

from sqlalchemy import asc, desc
from sqlalchemy.sql.elements import UnaryExpression


def query_for(filter_dict: Dict[str, any], order_dict: Dict[str, any]) -> Dict[str, any]:
    query_filter = {f"filters[{key}]": value for (key, value) in filter_dict.items()} if filter_dict is not None else {}
    query_order_by = {f"sort[{key}]": value for (key, value) in order_dict.items()} if order_dict is not None else {}
    return {**query_filter, **query_order_by}


def parse_query_filter(args: bytes) -> Dict[str, Any]:
    r"""
    Parses filters from query arguments.

    :param args: The raw query string returned by request.query_string
    :return: dictionary of attributes with values
    """
    return dict([
        (__attr_from_query(q[0].decode('utf-8')), __parse_value(q[1].decode('utf-8')))
        for q in parse_qsl(args)
        if q[0].startswith(b'filters')
    ])


def parse_query_order_by(args: bytes) -> OrderedDict[str, UnaryExpression]:
    r"""
    Parses sort order from query arguments.

    :param args: The raw query string returned by request.query_string
    :return: dictionary of attributes with sort orders for the same attributes
    """
    return collections.OrderedDict([
        __parse_order_by_value(__attr_from_query(q[0].decode('utf-8')), q[1].decode('utf-8'))
        for q in parse_qsl(args)
        if q[0].startswith(b'sort')
    ])


def __attr_from_query(key: str) -> str:
    r"""
    Gets the attribute from the query string key. For example 'filter[created_at]' wil result in 'created_at'.

    :param key: query string key
    :return: the attribute related to this key, if there are no [] then returns the full key
    """
    try:
        start_index = key.index('[') + 1
        end_index = key.index(']', start_index)
        return key[start_index:end_index]
    except ValueError:
        return key


def __parse_value(value: str) -> Any:
    r"""
    Parses the string to any other valid type like int, float, boolean, date or just a string
    :param value: the query string value
    :return: value in it's respective data type or a string if no valid data type can be found
    """
    if value.lower() in ['null', 'none']:
        return None
    try:
        return int(value)
    except ValueError:
        pass
    try:
        return float(value)
    except ValueError:
        pass
    try:
        return datetime.fromisoformat(value).astimezone(timezone.utc)
    except ValueError:
        pass
    return value


def __parse_order_by_value(key: str, value: str) -> (str, UnaryExpression):
    r"""
    Determines how to sort the attribute specified by key based on the value.

    :param key: attribute to be sorted
    :param value: query string value
    :return: key, desc(key) iff value.lower() == 'desc' otherwise asc(key)
    """
    if value is not None and value.lower() == 'desc':
        return key, desc(key)
    return key, asc(key)
