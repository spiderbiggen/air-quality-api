from functools import wraps

from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, jwt_optional, get_jwt_claims, verify_jwt_in_request

from app import jwt

def role_required(role):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            verify_jwt_in_request()
            user_role = get_jwt_claims()['role']
            if user_role < role:
                return bad_request('Insufficient Permissions!')
            else:
                return func(*args, **kwargs)
        return wrapper
    return decorator

@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {'role': user.role}

@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.email
