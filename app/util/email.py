from os.path import join
from threading import Thread
from typing import List

from flask import render_template, current_app, copy_current_request_context
from flask_mail import Message

from app import mail
from app.models import User


def send_email(subject: str, recipients: List[str], text_body, html_body, sender: str = None):
    msg = Message(subject, recipients=recipients, sender=sender)
    msg.body = text_body
    msg.html = html_body
    msg.attach(
        'logo.png', 'image/png',
        open(join('app', 'static', 'images', 'logo', 'AQM-logo-trans.png'), 'rb').read(), 'inline',
        headers=[['Content-ID','<Logo>'],]
    )

    @copy_current_request_context
    def send_async_email(message: Message):
        mail.send(message)

    Thread(name='mail_sender', target=send_async_email, args=(msg,)).start()


def send_password_reset_email(user: User):
    token = user.get_reset_password_token()
    subject = '[AQM] Reset Your Password'
    args = {
        'subject': subject,
        'user': user, 'token': token,
        'timeout': '15 minutes',
        'frontend': current_app.config['FRONTEND_URL'] or '',
    }
    send_email(
        subject,
        recipients=[user.email],
        text_body=render_template('email/reset_password.txt', **args),
        html_body=render_template('email/reset_password.html', **args)
    )


def send_account_activation_email(user: User):
    token = user.get_reset_password_token()
    subject = '[AQM] Activate your account'
    args = {
        'subject': subject,
        'user': user, 'token': token,
        'timeout': '24 hours',
        'frontend': current_app.config['FRONTEND_URL'] or '',
    }
    send_email(
        subject,
        recipients=[user.email],
        text_body=render_template('email/activation.txt', **args),
        html_body=render_template('email/activation.html', **args)
    )
