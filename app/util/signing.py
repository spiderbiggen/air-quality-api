from typing import Dict, Any

from flask import current_app
from itsdangerous.url_safe import URLSafeTimedSerializer

serializer = None


def __get_serializer():
    global serializer
    if serializer is None:
        serializer = URLSafeTimedSerializer(current_app.config['JWT_SECRET_KEY'])
    return serializer


def serialize_timed_value(value: Dict[str, Any], salt=None) -> str:
    return __get_serializer().dumps(value, salt=salt)


def validate_timed_value(value: bytes, max_age: int = 300, salt=None) -> Dict[str, Any]:
    return __get_serializer().loads(value, max_age, salt=salt)
