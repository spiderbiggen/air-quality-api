from datetime import datetime
from distutils.util import strtobool
from typing import Set, List

from flask import url_for
from sqlalchemy import or_, and_

from app import db
from app.mixins import PaginatedAPIMixin, TransformMixin, UuidMixin, FilterMixin
from app.models import Building, Device


class Floor(FilterMixin, PaginatedAPIMixin, UuidMixin, TransformMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), index=True, unique=True)
    building_id = db.Column(db.Integer, db.ForeignKey('building.id'), nullable=False)
    building = db.relationship('Building', back_populates='floors')
    level = db.Column(db.Integer, nullable=False)
    image_id = db.Column(db.Integer, db.ForeignKey('media.id'))
    image = db.relationship('Media')
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    devices = db.relationship('Device', back_populates='floor', lazy='dynamic')

    __table_args__ = (
        db.UniqueConstraint('building_id', 'level', name='_building_level_uc'),
    )

    def __repr__(self):
        return '<Floor {} of Building {}>'.format(self.level, self.building_id)

    def to_dict(self, includes: List[str] = None, **kwargs):
        data = {
            'id': self.uuid,
            'building_id': self.building.uuid,
            'level': self.level,
            'created_at': self.created_at.isoformat() + 'Z',
            'updated_at': self.updated_at.isoformat() + 'Z',
        }
        if includes is not None:
            if 'building' in includes:
                data['building'] = self.building.to_dict()
            if 'devices' in includes:
                now = datetime.utcnow()
                items = self.devices.filter(and_(
                    Device.valid_at <= now,
                    or_(Device.disabled_at == None, Device.disabled_at >= now)
                ))
                device_includes = [item[8:] for item in includes if item.startswith('devices.')]
                data['devices'] = [item.to_dict(includes=device_includes) for item in items]
            if 'stats' in includes:
                device_count = Device.query \
                    .filter(Device.floor_id == self.id) \
                    .count()
                data['stats'] = {'device_count': device_count, 'score': 100}
        data['_links'] = {
            'self': url_for('floors.get_floor', uuid=self.uuid),
            'image': url_for('floors.get_floor_image', uuid=self.uuid) if self.image is not None else None,
        }
        return data

    def from_dict(self, data, new_floor=False):
        for field in ['building_id', 'level']:
            if field in data:
                setattr(self, field, data[field])
        if new_floor:
            self.uuid = Floor.generate_uuid()

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        return super().__get_filter_keywords__().union({'uuid', 'level', 'building_id', 'has_image'})

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        if 'building_id' in kwargs:
            building = Building.query.filter_by(uuid=kwargs['building_id']).first()
            query = query.filter_by(building_id=building.id if building is not None else None)
            del kwargs['building_id']
        if 'has_image' in kwargs:
            try:
                should_have_image = bool(strtobool(kwargs['has_image']))
                if should_have_image:
                    query = query.filter(cls.image_id != None)
                else:
                    query = query.filter(cls.image_id == None)
            except ValueError:
                pass
                # do nothing
            del kwargs['has_image']
        return super().__filter__(query, **kwargs)
