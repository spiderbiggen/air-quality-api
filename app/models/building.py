from datetime import datetime
from distutils.util import strtobool
from typing import Set

from flask import url_for
from app import db
from app.mixins import PaginatedAPIMixin, UuidMixin, TransformMixin, FilterMixin


class Building(FilterMixin, PaginatedAPIMixin, UuidMixin, TransformMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), index=True, unique=True)
    name = db.Column(db.String(255), index=True, unique=True, nullable=False)
    postal_code = db.Column(db.String(6))
    house_number = db.Column(db.Integer)
    house_number_addition = db.Column(db.String(8))
    image_id = db.Column(db.Integer, db.ForeignKey('media.id'))
    image = db.relationship('Media')
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    floors = db.relationship('Floor', back_populates='building', lazy='dynamic')

    def __repr__(self):
        return '<Building {}>'.format(self.name)

    def to_dict(self, includes=None, **kwargs):
        data = {
            'id': self.uuid,
            'name': self.name,
            'postal_code': self.postal_code,
            'house_number': self.house_number,
            'house_number_addition': self.house_number_addition,
            'created_at': self.created_at.isoformat() + 'Z',
            'updated_at': self.updated_at.isoformat() + 'Z',
        }
        if includes is not None:
            if 'floors' in includes:
                floor_includes = [key[7:] for key in includes if key.startswith('floors.')]
                data['floors'] = [item.to_dict(includes=floor_includes) for item in self.floors.order_by('level')]
            if 'stats' in includes:
                from app.models import Floor
                from app.models import Device
                device_count = Device.query \
                    .join(Device.floor).join(Floor.building) \
                    .filter(Floor.building_id == self.id) \
                    .count()
                data['stats'] = {'device_count': device_count, 'score': 100}
        data['_links'] = {
            'self': url_for('buildings.get_building', uuid=self.uuid),
            'image': url_for('buildings.get_building_image', uuid=self.uuid) if self.image is not None else None,
        }
        return data

    def from_dict(self, data, new_building=False):
        for field in ['name', 'postal_code', 'house_number', 'house_number_addition']:
            if field in data:
                setattr(self, field, data[field])
        if new_building:
            self.uuid = Building.generate_uuid()

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        return super().__get_filter_keywords__().union(
            {'uuid', 'name', 'postal_code', 'house_number', 'house_number_addition', 'has_image'}
        )

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        if 'name' in kwargs:
            query = query.filter(cls.name.like(f'%{kwargs["name"]}%'))
            del kwargs['name']
        if 'has_image' in kwargs:
            try:
                should_have_image = bool(strtobool(kwargs['has_image']))
                if should_have_image:
                    query = query.filter(cls.image_id != None)
                else:
                    query = query.filter(cls.image_id == None)
            except ValueError:
                pass
                # do nothing
            del kwargs['has_image']
        return super().__filter__(query, **kwargs)
