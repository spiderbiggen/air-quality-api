import json
from datetime import datetime, timedelta, timezone
from typing import Set, List, Dict

from dateutil.relativedelta import relativedelta
from flask import url_for
from pandas import DataFrame
import pandas as pd
from sqlalchemy import asc

from app import db
from app.mixins import PaginatedAPIMixin, UuidMixin, TransformMixin, FilterMixin

interval_set = {'year', 'month', 'week', 'day'}


class Device(FilterMixin, PaginatedAPIMixin, UuidMixin, TransformMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), index=True, unique=True)
    floor_id = db.Column(db.Integer, db.ForeignKey('floor.id'))
    valid_at = db.Column(db.DateTime)
    area_name = db.Column(db.String(255))
    x_position = db.Column(db.Float)
    y_position = db.Column(db.Float)
    is_outside = db.Column(db.Boolean)
    area_usage = db.Column(db.String(255))
    disabled_at = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    history = db.relationship('DeviceHistory', back_populates='device', lazy='dynamic')
    data = db.relationship('SensorData', back_populates='device')
    floor = db.relationship('Floor', back_populates='devices')

    def __repr__(self):
        return '<Device {}>'.format(self.uuid)

    @staticmethod
    def __get_interval(interval: str) -> (datetime, timedelta):
        now = datetime.now(timezone.utc)
        if interval == 'year':
            return now - relativedelta(years=1), timedelta(days=1)
        if interval == 'month':
            return now - relativedelta(months=1), timedelta(hours=3)
        if interval == 'week':
            return now - relativedelta(weeks=1), timedelta(hours=1)
        return now - relativedelta(days=1), timedelta(minutes=10)

    def __grouped_sensor_data(self, interval: str) -> List[Dict[str, any]]:
        from app.models import SensorData
        start_date, interval = Device.__get_interval(interval)
        data = SensorData.query.filter_by(device_id=self.id) \
            .filter(SensorData.created_at >= start_date) \
            .order_by(asc('created_at')) \
            .all()
        if len(data) == 0:
            return data
        data = [item.to_dict() for item in data]
        df = DataFrame(data)
        df['created_at'] = pd.to_datetime(df['created_at'])
        df2 = df.resample(interval, on='created_at', kind='timestamp', origin=start_date).mean()
        df2['created_at'] = df2.index
        return json.loads(df2.to_json(orient='records', date_format='iso'))

    def to_dict(self, includes=None, include_position=True, **kwargs):
        data = {
            'id': self.uuid,
            'device_id': self.id,
            'disabled_at': self.disabled_at.isoformat() + 'Z' if self.disabled_at else None,
            'created_at': self.created_at.isoformat() + 'Z',
            'updated_at': self.updated_at.isoformat() + 'Z',
        }
        if include_position:
            data['floor_id'] = self.floor_id
            data['valid_at'] = self.valid_at.isoformat() + 'Z' if self.valid_at else None
            data['area_name'] = self.area_name
            data['x_position'] = self.x_position
            data['y_position'] = self.y_position
            data['is_outside'] = self.is_outside
            data['area_usage'] = self.area_usage
        if includes is not None:
            intervals = interval_set.intersection(includes)
            if 'floor' in includes:
                data['floor'] = self.floor.to_dict() if self.floor is not None else None
            if len(intervals) != 0:
                data['sensor_data'] = self.__grouped_sensor_data(intervals.pop())
        data['_links'] = {
            'self': url_for('devices.get_device', uuid=self.uuid),
        }
        return data

    def from_dict(self, data, new_device: bool = False, link: bool = False):
        if new_device:
            if 'id' not in data:
                raise ValueError('A device requires an id')
            self.id = data['id']
            self.uuid = Device.generate_uuid()
        keys = ['disabled_at']
        if link:
            keys.extend(['valid_at', 'valid_till', 'area_name', 'x_position', 'y_position', 'is_outside', 'area_usage'])
        for field in keys:
            if field in data:
                setattr(self, field, data[field])

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        return super().__get_filter_keywords__().union(
            {'uuid', 'disabled_at', 'floor_id', 'area_usage', 'is_outside', 'valid_at'}
        )

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        if 'floor_id' in kwargs:
            from app.models import Floor
            floor = Floor.query.filter_by(uuid=kwargs['floor_id']).first()
            query = query.filter_by(floor_id=floor.id if floor is not None else None)
            del kwargs['floor_id']
        return super().__filter__(query, **kwargs)
