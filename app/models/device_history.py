from datetime import datetime
from typing import Set

from flask import url_for

from app import db
from app.mixins import TransformMixin, UuidMixin, FilterMixin, PaginatedAPIMixin


class DeviceHistory(PaginatedAPIMixin, FilterMixin, UuidMixin, TransformMixin, db.Model):
    uuid = db.Column(db.String(36), unique=True)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    floor_id = db.Column(db.Integer, db.ForeignKey('floor.id'), nullable=False)
    valid_from = db.Column(db.DateTime, nullable=False, index=True)
    valid_till = db.Column(db.DateTime, index=True)
    area_name = db.Column(db.String(255))
    x_position = db.Column(db.Integer)
    y_position = db.Column(db.Integer)
    is_outside = db.Column(db.Boolean, nullable=False, default=False)
    area_usage = db.Column(db.String(255), nullable=False)
    disabled_at = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    device = db.relationship('Device', back_populates='history')
    floor = db.relationship('Floor')
    __table_args__ = (
        db.PrimaryKeyConstraint('device_id', 'floor_id', 'valid_from', name='pk_device_floor_from'),
    )

    def __repr__(self):
        return '<DeviceHistory {}>'.format(self.uuid)

    def to_dict(self, includes=None, **kwargs):
        data = {
            'id': self.uuid,
            'device_id': self.device.uuid,
            'floor_id': self.floor.uuid,
            'valid_from': self.valid_from.isoformat() + 'Z',
            'valid_till': self.valid_till.isoformat() + 'Z' if self.valid_till else None,
            'area_name': self.area_name,
            'x_position': self.x_position,
            'y_position': self.y_position,
            'is_outside': self.is_outside,
            'area_usage': self.area_usage,
            'created_at': self.created_at.isoformat() + 'Z',
            'updated_at': self.updated_at.isoformat() + 'Z',
        }
        if includes is not None:
            if 'device' in includes:
                data['device'] = self.device.to_dict(include_position=False, **kwargs)
            if 'floor' in includes:
                data['floor'] = self.floor.to_dict(**kwargs)
        data['_links'] = {
            'self': url_for('device_history.get_device_on_floor', uuid=self.uuid),
        }
        return data

    def from_dict(self, data, new_link=False, **kwargs):
        for field in ['valid_from', 'valid_till', 'area_name', 'x_position',
                      'y_position', 'is_outside', 'area_usage']:
            if field in data:
                setattr(self, field, data[field])
        if new_link:
            self.uuid = DeviceHistory.generate_uuid()

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        return super().__get_filter_keywords__().union({})

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        return super().__filter__(query, **kwargs)
