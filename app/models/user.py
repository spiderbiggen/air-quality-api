from __future__ import annotations

import re
from datetime import datetime, timezone
from email.utils import parseaddr
from typing import Set, Optional, Dict, Any

from flask import url_for
from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from app.mixins import PaginatedAPIMixin, UuidMixin, TransformMixin, FilterMixin
from app.models.enums import Role
from app.util.signing import serialize_timed_value, validate_timed_value


password_regex = re.compile('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{7,}$')


class User(FilterMixin, PaginatedAPIMixin, UuidMixin, TransformMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), index=True, unique=True)
    email = db.Column(db.String(255), index=True, unique=True, nullable=False)
    role = db.Column(db.Enum(Role), nullable=False, default=Role.USER)
    password_hash = db.Column(db.String(128), nullable=True)
    expires_at = db.Column(db.DateTime)
    disabled_at = db.Column(db.DateTime)
    activated_at = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<User {}>'.format(self.email)

    def set_password(self, password):
        if password is None:
            self.password_hash = None
        else:
            self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def to_dict(self, **kwargs):
        return {
            'id': self.uuid,
            'email': self.email,
            'role': self.role.name.lower(),
            'expires_at': self.expires_at.isoformat() + 'Z' if self.expires_at is not None else None,
            'disabled_at': self.disabled_at.isoformat() + 'Z' if self.disabled_at is not None else None,
            'activated_at': self.activated_at.isoformat() + 'Z' if self.activated_at is not None else None,
            'created_at': self.created_at.isoformat() + 'Z',
            'updated_at': self.updated_at.isoformat() + 'Z',
            '_links': {
                'self': url_for('users.get_user', uuid=self.uuid),
            }
        }

    @classmethod
    def validate_invalid_dict_values(cls, data: Dict[str, Any], ignore_password=False, **kwargs):
        super().validate_invalid_dict_values(data, )
        if 'email' in data:
            (_, mail) = parseaddr(data['email'])
            if len(mail) == 0:
                del data['email']
        if not ignore_password and 'password' in data:
            if password_regex.match(data['password']) is None:
                data['password'] = None

    def from_dict(self, data: Dict[str, Any], new_user=False):
        for key, value in data.items():
            if key in ['expires_at', 'disabled_at']:
                setattr(self, key, datetime.fromisoformat(value).astimezone(timezone.utc) if value is not None else None)
        if 'email' in data:
            email = str(data['email']).lower()
            self.email = email
        if 'password' in data:
            self.set_password(data['password'])
        if 'role' in data:
            self.role = Role[str(data['role']).upper()]
        if new_user:
            self.uuid = User.generate_uuid()

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        return super().__get_filter_keywords__().union({'uuid', 'email', 'role', 'expires_at', 'disabled_at'})

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        if 'email' in kwargs:
            query = query.filter(cls.email.ilike(f'%{kwargs["email"]}%'))
            del kwargs['email']
        return super().__filter__(query, **kwargs)

    def get_reset_password_token(self) -> str:
        body = {'reset_password': self.uuid}
        return serialize_timed_value(body)

    @classmethod
    def verify_reset_password_token(cls, token, max_age: int = 900) -> Optional[User]:
        try:
            uuid = validate_timed_value(token, max_age)['reset_password']
        except Exception as e:
            print(e)
            return None
        return cls.query.filter_by(uuid=uuid).first()
