from datetime import datetime
from typing import Set

from app import db
from app.mixins import PaginatedAPIMixin, TransformMixin, FilterMixin
from app.models import Floor


class SensorData(FilterMixin, PaginatedAPIMixin, TransformMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    co2 = db.Column(db.Float)
    co = db.Column(db.Float)
    voc = db.Column(db.Float)
    pm2_5 = db.Column(db.Float)
    temperature = db.Column(db.Float)
    humidity = db.Column(db.Float)
    battery = db.Column(db.Float)
    invalid_from = db.Column(db.DateTime)
    device = db.relationship('Device', back_populates='data')
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<SensorData {} at {}>'.format(self.device.uuid, self.created_at)

    def to_dict(self, **kwargs):
        return {
            'device_id': self.device.uuid,
            'co2': self.co2,
            'co': self.co,
            'voc': self.voc,
            'pm2_5': self.pm2_5,
            'temperature': self.temperature,
            'humidity': self.humidity,
            'battery': self.battery,
            'invalid_from': self.invalid_from.isoformat() + 'Z' if self.invalid_from else None,
            'created_at': self.created_at.isoformat() + 'Z',
            'updated_at': self.updated_at.isoformat() + 'Z',
        }

    def from_dict(self, data, **kwargs):
        for field in ['device_id', 'co2', 'co', 'voc', 'pm2_5', 'temperature', 'humidity', 'battery', 'invalid_from']:
            if field in data:
                setattr(self, field, data[field])

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        return super().__get_filter_keywords__().union({'device_id', 'device.floor_id'})

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        if 'device.floor_id' in kwargs:
            uuid = Floor.query.filter_by(uuid=kwargs['device.floor_id'])
            query = query.join(SensorData.device)
            query = query.filter(SensorData.device.floor_id == uuid)
            del kwargs['has_image']
        return super().__filter__(query, **kwargs)




