from enum import IntEnum


class Role(IntEnum):
    USER: int = 0,
    RESEARCHER: int = 1,
    MANAGER: int = 2,
    ADMIN: int = 3
