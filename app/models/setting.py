from datetime import datetime
from typing import Set

from flask import url_for
from app import db
from app.mixins import PaginatedAPIMixin, TransformMixin, FilterMixin


class Setting(FilterMixin, PaginatedAPIMixin, TransformMixin, db.Model):
    key = db.Column(db.String(255), primary_key=True)
    value = db.Column(db.String(255), nullable=False)
    type = db.Column(db.String(255), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Setting ({}){}: {}>'.format(self.type, self.key, self.value)

    def to_dict(self, **kwargs):
        return {
            'key': self.key,
            'value': self.value,
            'type': self.type,
            'created_at': self.created_at.isoformat() + 'Z',
            'updated_at': self.updated_at.isoformat() + 'Z',
            '_links': {
                'self': url_for('settings.get_setting', key=self.key),
            }
        }

    def from_dict(self, data, **kwargs):
        # TODO ADD validation
        for field in ['key', 'value', 'type']:
            if field in data:
                setattr(self, field, data[field])

    @classmethod
    def __get_filter_keywords__(cls) -> Set[str]:
        return super().__get_filter_keywords__().union({'key', 'type'})

    @classmethod
    def __filter__(cls, query: db.Query, **kwargs) -> db.Query:
        if 'key' in kwargs:
            query = query.filter(cls.name.like(f'%{kwargs["key"]}%'))
            del kwargs['key']
        return super().__filter__(query, **kwargs)
