from flask import Flask
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_mqtt import Mqtt

from config import Config

db = SQLAlchemy()
migrate = Migrate()
cors = CORS()
mail = Mail()
jwt = JWTManager()
mqtt = Mqtt()


def create_app(config_class=Config):
    from .services import mqtt_client
    app = Flask(__name__)
    app.config.from_object(config_class)
    app.url_map.strict_slashes = False

    db.init_app(app)
    migrate.init_app(app, db)
    cors.init_app(app)
    mail.init_app(app)
    jwt.init_app(app)
    #mqtt.init_app(app)

    # initialize models
    from app import models

    # initialize routes
    from .routes import register_blueprints
    register_blueprints(app)

    def get_resource_as_string(name, charset='utf-8'):
        with app.open_resource(name) as f:
            return f.read().decode(charset)

    app.jinja_env.globals['get_resource_as_string'] = get_resource_as_string
    
    return app
