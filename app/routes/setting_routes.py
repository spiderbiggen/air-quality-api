from flask_jwt_extended import jwt_required
from flask import Blueprint, jsonify, request, url_for, make_response, current_app

from app import db

from app.errors import bad_request
from app.models import Setting

settings = Blueprint('settings', __name__, url_prefix='/settings')


@settings.route('/', methods=['GET'])
@jwt_required
def get_settings():
    data = Setting.to_collection_dict(request, '.get_settings')
    return jsonify(data)


@settings.route('/', methods=['POST'])
@jwt_required
def create_setting():
    data = request.get_json() or {}
    Setting.validate_invalid_dict_values(data, )
    if 'key' not in data or 'value' not in data or 'type' not in data:
        return bad_request('must include key, value and type fields')
    setting = Setting()
    setting.from_dict(data)
    db.session.add(setting)
    db.session.commit()
    response = jsonify(setting.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('.get_setting', key=setting.key)
    return response


@settings.route('/<string:key>', methods=['GET'])
@jwt_required
def get_setting(key):
    return jsonify(Setting.query.filter_by(key=key).first_or_404().to_dict())


@settings.route('/<string:key>', methods=['PATCH'])
@jwt_required
def update_setting(key):
    setting = Setting.query.filter_by(key=key).first_or_404()
    data = request.get_json() or {}
    Setting.validate_invalid_dict_values(data, )
    if 'key' in data and data['key'] != setting.key:
        return bad_request('cannot change key')
    setting.from_dict(data)
    db.session.commit()
    return jsonify(setting.to_dict())


@settings.route('/<string:key>', methods=['DELETE'])
@jwt_required
def remove_setting(key):
    setting = Setting.query.filter_by(key=key).first_or_404()
    db.session.delete(setting)
    db.session.commit()
    response = make_response('', 204)
    response.mimetype = current_app.config['JSONIFY_MIMETYPE']

    return response
