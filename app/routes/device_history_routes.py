
from flask_jwt_extended import jwt_required
from flask import Blueprint, jsonify, request

from app.models import DeviceHistory

device_history = Blueprint('devices_history', __name__, url_prefix='/device_history')


@device_history.route('/', methods=['GET'])
@jwt_required
def get_device_history():
    data = DeviceHistory.to_collection_dict(request, '.get_device_history')
    return jsonify(data)


@device_history.route('/<string:uuid>', methods=['GET'])
@jwt_required
def get_device_history_by_device_id(uuid: str):
    active = DeviceHistory.query.filter_by(uuid=uuid).first_or_404()
    includes = request.args.getlist('with[]')
    return jsonify(active.to_dict(includes=includes))
