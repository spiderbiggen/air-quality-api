from .building_routes import buildings
from .device_routes import devices
from .device_history_routes import device_history
from .floor_routes import floors
from .sensor_data_routes import sensor_data
from .setting_routes import settings
from .user_routes import users


def register_blueprints(app):
    app.register_blueprint(users)
    app.register_blueprint(settings)
    app.register_blueprint(buildings)
    app.register_blueprint(floors)
    app.register_blueprint(devices)
    app.register_blueprint(sensor_data)
