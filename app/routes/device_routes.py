from datetime import datetime

from flask_jwt_extended import jwt_required
from flask import Blueprint, jsonify, request, url_for, make_response, current_app

from app import db
from app.errors import not_found, bad_request
from app.models import Device, SensorData, Floor
from app.models.device_history import DeviceHistory

devices = Blueprint('devices', __name__, url_prefix='/devices')


@devices.route('/', methods=['GET'])
@jwt_required
def get_devices():
    data = Device.to_collection_dict(request, '.get_devices')
    return jsonify(data)


@devices.route('/', methods=['POST'])
@jwt_required
def create_device():
    data = request.get_json() or {}
    Device.validate_invalid_dict_values(data, )
    device = Device()
    device.from_dict(data, new_device=True)
    db.session.add(device)
    db.session.commit()
    includes = request.args.getlist('with[]')
    response = jsonify(device.to_dict(includes=includes))
    response.status_code = 201
    response.headers['Location'] = url_for('.get_device', uuid=device.uuid)
    return response


@devices.route('/<string:uuid>', methods=['GET'])
@jwt_required
def get_device(uuid):
    includes = request.args.getlist('with[]')
    return jsonify(Device.query.filter_by(uuid=uuid).first_or_404().to_dict(includes=includes))


@devices.route('/<string:uuid>', methods=['PATCH'])
@jwt_required
def update_device(uuid):
    device = Device.query.filter_by(uuid=uuid).first_or_404()
    data = request.get_json() or {}
    Device.validate_invalid_dict_values(data, )
    device.from_dict(data)
    db.session.commit()
    includes = request.args.getlist('with[]')
    return jsonify(device.to_dict(includes=includes))


@devices.route('/<string:uuid>', methods=['DELETE'])
@jwt_required
def remove_device(uuid):
    device = Device.query.filter_by(uuid=uuid).first_or_404()
    db.session.delete(device)
    db.session.commit()
    response = make_response('', 204)
    response.mimetype = current_app.config['JSONIFY_MIMETYPE']
    return response


@devices.route('/<string:uuid>/sensor_data', methods=['GET'])
@jwt_required
def get_device_sensor_data(uuid):
    device = Device.query.filter_by(uuid=uuid).first_or_404()
    query = SensorData.query.filter_by(device_id=device.id)
    data = SensorData.to_collection_dict(query, '.get_device_sensor_data', query=query, uuid=uuid, filter_deny={'device_id'})
    return jsonify(data)


@devices.route('/<string:uuid>/floor', methods=['GET'])
@jwt_required
def get_active_floor_for_device(uuid):
    device = Device.query.filter_by(uuid=uuid).first_or_404()
    includes = request.args.getlist('with[]')
    return jsonify(device.floor.to_dict(includes=includes))


@devices.route('/<string:uuid>/floor/<string:floor_uuid>', methods=['POST'])
@jwt_required
def link_device_to_floor(uuid, floor_uuid):
    data = request.get_json() or {}
    Device.validate_invalid_dict_values(data, )
    if 'area_usage' not in data:
        return bad_request('Make sure that the area usage field is set')

    device = Device.query.filter_by(uuid=uuid).first_or_404()
    floor = Floor.query.filter_by(uuid=floor_uuid).first_or_404()
    history = DeviceHistory()
    utc_now = datetime.utcnow()

    history.from_dict(device.to_dict(), new_link=True)
    history.device_id = device.id
    history.floor_id = floor.id
    history.valid_till = utc_now

    device.from_dict(data)
    device.valid_at = utc_now

    db.session.add(history)
    db.session.commit()
    return jsonify(device.to_dict())
