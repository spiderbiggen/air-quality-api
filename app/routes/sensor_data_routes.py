from flask_jwt_extended import jwt_required
from flask import Blueprint, jsonify, request

from app.models import SensorData

sensor_data = Blueprint('sensor_data', __name__, url_prefix='/sensor_data')


@sensor_data.route('/', methods=['GET'])
@jwt_required
def get_sensor_data():
    data = SensorData.to_collection_dict(request, '.get_sensor_data')
    return jsonify(data)
