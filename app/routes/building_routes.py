from io import BytesIO

from flask_jwt_extended import jwt_required, jwt_optional
from flask import Blueprint, jsonify, request, url_for, make_response, current_app, send_file
from app import db

from app.errors import bad_request, not_found
from app.models import Building, Floor
from app.services import resize_image, media_from_file_storage
from app.util.decorator import role_required

buildings = Blueprint('buildings', __name__, url_prefix='/buildings')


@buildings.route('/', methods=['GET'])
@role_required(role=0)
def get_buildings():
    data = Building.to_collection_dict(request, '.get_buildings')
    return jsonify(data)


@buildings.route('/', methods=['POST'])
@role_required(role=2)
def create_building():
    data = request.get_json() or {}
    Building.validate_invalid_dict_values(data, )
    if 'name' not in data:
        return bad_request('must include name field')
    building = Building()
    building.from_dict(data, new_building=True)
    db.session.add(building)
    db.session.commit()
    includes = request.args.getlist('with[]')
    response = jsonify(building.to_dict(includes=includes))
    response.status_code = 201
    response.headers['Location'] = url_for('.get_building', uuid=building.uuid)
    return response


@buildings.route('/<string:uuid>', methods=['GET'])
@role_required(role=0)
def get_building(uuid):
    includes = request.args.getlist('with[]')
    return jsonify(Building.query.filter_by(uuid=uuid).first_or_404().to_dict(includes=includes))


@buildings.route('/<string:uuid>/image', methods=['GET'])
@jwt_optional
def get_building_image(uuid):
    image = Building.query.filter_by(uuid=uuid).first_or_404().image
    if image is None:
        return not_found()
    buffer = BytesIO()
    buffer.write(image.media)
    buffer.seek(0)
    return send_file(buffer, mimetype=image.mimetype, attachment_filename=image.file_name)


@buildings.route('/<string:uuid>/image', methods=['POST'])
@role_required(role=2)
def add_building_image(uuid):
    uploaded = request.files['image']
    if uploaded is None:
        bad_request('A file is required')

    building = Building.query.filter_by(uuid=uuid).first_or_404()
    old_image = building.image
    building.image = resize_image(media_from_file_storage(file=uploaded))

    if old_image is not None:
        db.session.delete(old_image)
    db.session.add(building.image)
    db.session.commit()

    response = make_response()
    response.status_code = 201
    response.headers['Location'] = url_for('.get_building_image', uuid=building.uuid)
    return response


@buildings.route('/<string:uuid>', methods=['PATCH'])
@role_required(role=2)
def update_building(uuid):
    building = Building.query.filter_by(uuid=uuid).first_or_404()
    data = request.get_json() or {}
    Building.validate_invalid_dict_values(data, )
    building.from_dict(data)
    db.session.commit()
    includes = request.args.getlist('with[]')
    return jsonify(building.to_dict(includes=includes))


@buildings.route('/<string:uuid>', methods=['DELETE'])
@role_required(role=2)
def remove_building(uuid):
    building = Building.query.filter_by(uuid=uuid).first_or_404()
    db.session.delete(building)
    db.session.commit()
    response = make_response('', 204)
    response.mimetype = current_app.config['JSONIFY_MIMETYPE']

    return response


@buildings.route('/<string:uuid>/floors', methods=['GET'])
@role_required(role=0)
def get_building_floors(uuid):
    building = Building.query.filter_by(uuid=uuid).first_or_404()
    query = Floor.query.filter(Floor.building == building)
    data = Floor.to_collection_dict(request, '.get_building_floors', query=query, uuid=uuid,
                                    filter_deny={'building_id'})
    return jsonify(data)


@buildings.route('/<string:uuid>/floors', methods=['POST'])
@role_required(role=2)
def create_building_floors(uuid):
    building = Building.query.filter_by(uuid=uuid).first_or_404()
    data = request.get_json() or {}
    data['building_id'] = building.id
    if 'level' not in data:
        return bad_request('must include name field')
    if building.floors.filter_by(level=data['level']).first() is not None:
        return bad_request('this level already exists for this building')
    floor = Floor()
    floor.from_dict(data, new_floor=True)
    db.session.add(floor)
    db.session.commit()
    includes = request.args.getlist('with[]')
    response = jsonify(floor.to_dict(includes=includes))
    response.status_code = 201
    response.headers['Location'] = url_for('floors.get_floor', uuid=floor.uuid)
    return response
