from datetime import datetime

from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_optional, get_jwt_claims
from flask import Blueprint, jsonify, request, url_for, make_response, current_app
from app import db

from app.errors import bad_request
from app.models import User
from app.util.email import send_password_reset_email, send_account_activation_email
from app.util.decorator import role_required

users = Blueprint('users', __name__, url_prefix='/users')


@users.route('/', methods=['GET'])
@role_required(role=3)
def get_users():
    data = User.to_collection_dict(request, '.get_users')
    return jsonify(data)


@users.route('/', methods=['POST'])
@role_required(role=3)
def admin_create_user():
    data = request.get_json() or {}
    User.validate_invalid_dict_values(data, )
    if 'email' not in data:
        return bad_request('Must include e-mail')
    if 'password' in data and data['password'] is None:
        return bad_request('Please enter a valid password')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('Please use a different email address')
    user = User()
    user.from_dict(data, new_user=True)
    db.session.add(user)
    db.session.commit()
    send_account_activation_email(user)

    response = jsonify(user.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('.get_user', uuid=user.id)
    return response


@users.route('/register', methods=['POST'])
@jwt_optional
def create_user():
    data = request.get_json() or {}
    User.validate_invalid_dict_values(data, )
    if 'password' not in data or 'email' not in data:
        return bad_request('Must include e-mail and password fields')
    if 'password' in data and data['password'] is None:
        return bad_request('Please enter a valid password')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('Please use a different email address')
    user = User()
    user.role = Role.USER
    user.from_dict(data, new_user=True)
    db.session.add(user)
    db.session.commit()
    send_account_activation_email(user)
    return jsonify(user.to_dict())


@users.route('/<string:uuid>', methods=['GET'])
@role_required(role=3)
def get_user(uuid):
    return jsonify(User.query.filter_by(uuid=uuid).first_or_404().to_dict())


@users.route('/<string:uuid>', methods=['PATCH'])
@role_required(role=3)
def update_user(uuid):
    user = User.query.filter_by(uuid=uuid).first_or_404()
    data = request.get_json() or {}
    User.validate_invalid_dict_values(data, )
    if 'email' in data and data['email'] != user.email and User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    if 'password' in data and data['password'] is None:
        return bad_request('please enter a valid password')
    user.from_dict(data, new_user=False)
    db.session.commit()
    return jsonify(user.to_dict())


@users.route('/<string:uuid>', methods=['DELETE'])
@role_required(role=3)
def remove_user(uuid):
    user = User.query.filter_by(uuid=uuid).first_or_404()
    db.session.delete(user)
    db.session.commit()
    response = make_response('', 204)
    response.mimetype = current_app.config['JSONIFY_MIMETYPE']

    return response


@users.route('/<string:uuid>/enable', methods=['POST'])
@role_required(role=3)
def enable_user(uuid):
    user = User.query.filter_by(uuid=uuid).first_or_404()
    user.disabled_at = None
    db.session.commit()
    return jsonify(user.to_dict())


@users.route('/<string:uuid>/disable', methods=['POST'])
@role_required(role=3)
def disable_user(uuid):
    user = User.query.filter_by(uuid=uuid).first_or_404()
    current_user = get_jwt_identity()
    if user.email == current_user:
        return bad_request('You cant disable your own account!')
    if user.disabled_at is None:
        user.disabled_at = datetime.utcnow()
    db.session.commit()
    return jsonify(user.to_dict())

@users.route('/login', methods=['POST'])
def login_user():
    data = request.get_json() or {}
    User.validate_invalid_dict_values(data, ignore_password=True)
    if 'password' not in data or 'email' not in data:
        return bad_request('Must include e-mail and password fields')
    user = User.query.filter_by(email=data['email']).first()
    password = data['password']
    if user is None or not user.check_password(password):
    # Returning a non-specific response to prevent e-mail enumeration 
        return bad_request('Invalid e-mail address or password')
    # Checking if the account is disabled
    if user.disabled_at is not None:
        return bad_request('Account disabled')
    # Creating an access token for the user
    if user.activated_at is None:
        return bad_request('Please activate your account before logging in!')
    access_token = create_access_token(identity=user)
    return jsonify(access_token=access_token)


@users.route('/current_user', methods=['GET'])
@role_required(role=0)
def current_user():
    current_user = get_jwt_identity()
    current_role = get_jwt_claims()['role']
    user = User.query.filter_by(email=current_user).first()
    return jsonify(user.to_dict())


@users.route('/change_password', methods=['POST'])
@role_required(role=0)
def change_password():
    user = get_jwt_identity()
    data = request.get_json() or {}
    if 'password' not in data or 'new_password' not in data:
        return bad_request('Must include old and new passwords!')
    if data['new_password'] != data['new_password_confirmation']:
        return bad_request('New password is not the same as the confirmation password!')
    data['password'] = data['new_password']
    User.validate_invalid_dict_values(data)
    if 'password' in data and data['password'] is None:
        return bad_request('Please choose a more complex password!')
    user.set_password(data['password'])
    db.session.commit()
    return jsonify(user.to_dict())


@users.route('/activate/<string:token>', methods=['POST'])
@jwt_optional
def activate_user(token: str):
    data = request.get_json() or {}
    user = User.verify_reset_password_token(token, 86_400)
    if user is None:
        return bad_request('The used token was not valid.')
    if user.password_hash is None:
        if 'password' not in data:
            return bad_request('please provide a new password.')
        user.set_password(data['password'])
    user.activated_at = datetime.utcnow()
    db.session.commit()
    return jsonify(user.to_dict())


@users.route('/<string:uuid>/reset', methods=['GET'])
@role_required(role=3)
def admin_reset_password(uuid):
    user = User.query.filter_by(uuid=uuid).first_or_404()
    current_user = get_jwt_identity()
    if user.email == current_user:
        return bad_request('Already logged in! You cant reset your own password!')
    empty_response = make_response('', 204)
    empty_response.mimetype = current_app.config['JSONIFY_MIMETYPE']
    if user is None:
        return empty_response
    send_password_reset_email(user)
    return empty_response


@users.route('/<string:uuid>/role', methods=['POST'])
@role_required(role=3)
def admin_change_role(uuid):
    user = User.query.filter_by(uuid=uuid).first_or_404()
    data = request.get_json() or {}
    current_user = get_jwt_identity()
    if user.email == current_user:
        return bad_request('You cant change your own role!')
    User.validate_invalid_dict_values(data, )
    user.from_dict(data, new_user=False)
    db.session.commit()
    return jsonify(user.to_dict())


@users.route('/reset_password', methods=['GET'])
@jwt_optional
def request_reset_password():
    args = request.args
    if 'email' not in args:
        return bad_request('Password reset email cannot be sent without email')
    user = User.query.filter_by(email=args['email']).first()
    empty_response = make_response('', 204)
    empty_response.mimetype = current_app.config['JSONIFY_MIMETYPE']
    if user is None:
        return empty_response
    send_password_reset_email(user)
    return empty_response

@users.route('/reset_password/<string:token>', methods=['POST'])
@jwt_optional
def reset_password(token: str):
    data = request.get_json() or {}
    user = User.verify_reset_password_token(token)
    if user is None:
        return bad_request('The used token was not valid.')
    if 'password' not in data:
        return bad_request('please provide a new password.')
    if data['new_password'] != data['new_password_confirmation']:
        return bad_request('New password is not the same as the confirmation password!')
    user.set_password(data['password'])
    db.session.commit()
    return jsonify(user.to_dict())
