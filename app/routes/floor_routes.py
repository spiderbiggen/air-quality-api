from io import BytesIO

from flask_jwt_extended import jwt_required, jwt_optional
from flask import Blueprint, jsonify, request, url_for, make_response, current_app, send_file

from app import db

from app.errors import bad_request, not_found
from app.models import Floor, Building
from app.services import media_from_file_storage, resize_image

floors = Blueprint('floors', __name__, url_prefix='/floors')


@floors.route('/', methods=['GET'])
@jwt_required
def get_floors():
    data = Floor.to_collection_dict(request, '.get_floors')
    return jsonify(data)


@floors.route('/', methods=['POST'])
@jwt_required
def create_floor():
    data = request.get_json() or {}
    Floor.validate_invalid_dict_values(data, )
    if 'building_id' not in data or 'level' not in data:
        return bad_request('must include building_id and level fields')
    building = Building.query.filter_by(uuid=data['building_id']).first()
    if building is None:
        return bad_request('building_id does not correspond with an actual building')
    if building.floors.filter_by(level=data['level']).first() is not None:
        return bad_request('this level already exists for this building')

    data['building_id'] = building.id
    floor = Floor()
    floor.from_dict(data, new_floor=True)
    db.session.add(floor)
    db.session.commit()
    includes = request.args.getlist('with[]')
    response = jsonify(floor.to_dict(includes=includes))
    response.status_code = 201
    response.headers['Location'] = url_for('.get_floor', uuid=floor.uuid)
    return response


@floors.route('/<string:uuid>', methods=['GET'])
@jwt_required
def get_floor(uuid):
    includes = request.args.getlist('with[]')
    return jsonify(Floor.query.filter_by(uuid=uuid).first_or_404().to_dict(includes=includes))


@floors.route('/<string:uuid>/image', methods=['GET'])
@jwt_optional
def get_floor_image(uuid):
    image = Floor.query.filter_by(uuid=uuid).first_or_404().image
    if image is None:
        return not_found()
    buffer = BytesIO()
    buffer.write(image.media)
    buffer.seek(0)
    return send_file(buffer, mimetype=image.mimetype, attachment_filename=image.file_name)


@floors.route('/<string:uuid>/image', methods=['POST'])
@jwt_required
def add_floor_image(uuid):
    uploaded = request.files['image']
    if uploaded is None:
        bad_request('A file is required')

    floor = Floor.query.filter_by(uuid=uuid).first_or_404()
    old_image = floor.image
    floor.image = resize_image(media_from_file_storage(file=uploaded))

    if old_image is not None:
        db.session.delete(old_image)
    db.session.add(floor.image)
    db.session.commit()

    response = make_response()
    response.status_code = 201
    response.headers['Location'] = url_for('.get_floor_image', uuid=floor.uuid)
    return response


@floors.route('/<string:uuid>/building', methods=['GET'])
@jwt_required
def get_floor_building(uuid):
    floor = Floor.query.filter_by(uuid=uuid).first_or_404()
    includes = request.args.getlist('with[]')
    return jsonify(floor.building.to_dict(includes=includes))


@floors.route('/<string:uuid>', methods=['PATCH'])
@jwt_required
def update_floor(uuid):
    floor = Floor.query.filter_by(uuid=uuid).first_or_404()
    data = request.get_json() or {}
    Floor.validate_invalid_dict_values(data, )
    if 'building_id' in data:
        building = Building.query.filter_by(uuid=data['building_id']).first()
        if building is None:
            return bad_request('building_id does not correspond with an actual building')
        data['building_id'] = building.id
    floor.from_dict(data)
    db.session.commit()
    includes = request.args.getlist('with[]')
    return jsonify(floor.to_dict(includes=includes))


@floors.route('/<string:uuid>', methods=['DELETE'])
@jwt_required
def remove_floor(uuid):
    floor = Floor.query.filter_by(uuid=uuid).first_or_404()
    db.session.delete(floor)
    db.session.commit()
    response = make_response('', 204)
    response.mimetype = current_app.config['JSONIFY_MIMETYPE']

    return response
