from app import mqtt, db
import json

__data_topic = 'iot/air_quality/data'


@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    print('on_connect client : {} userdata :{} flags :{} rc:{}'.format(client, userdata, flags, rc))
    mqtt.subscribe(__data_topic)


@mqtt.on_message()
def handle_message(client, userdata, message):
    json_data = json.loads(message.payload.decode())

    if message.topic == __data_topic:
        if 'device_id' not in json_data:
            return
        from app.models import Device, SensorData
        from air_quality import app
        with app.app_context():
            device = Device.query.filter_by(id=json_data['device_id']).first()
            if device is None or device.disabled_at is not None:
                return
            data = SensorData()
            data.from_dict(json_data)
            db.session.add(device)
            db.session.add(data)
            db.session.commit()
    else:
        print(message.topic, json_data)


# @mqtt.on_log()
# def handle_logging(client, userdata, level, buf):
#     print(level, buf)
