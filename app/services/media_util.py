import io

from PIL import Image
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

from app.exceptions import MediaIsNotImageException
from app.models import Media

RESIZABLE_MIME_TYPES = ['jpeg', 'png']
RGB_FILE_TYPES = ['jpeg']


def media_from_file_storage(file: FileStorage) -> Media:
    if not file.mimetype.startswith('image/'):
        raise MediaIsNotImageException('invalid mimetype')
    image = Media()
    image.media = file.read()
    image.mimetype = file.mimetype
    image.file_name = secure_filename(file.filename)
    return image


def resize_image(image: Media, max_file_size: int = 2_048_000, tolerance: float = 0.05) -> Media:
    file_format = image.mimetype.rsplit('/')[1]
    if file_format not in RESIZABLE_MIME_TYPES:
        return image
    mode = 'RGB' if (file_format in RGB_FILE_TYPES) else 'RGBA'
    with io.BytesIO(image.media) as stream:
        img = img_orig = Image.open(stream).convert(mode)

    aspect = img.size[0] / img.size[1]

    while True:
        with io.BytesIO() as buffer:
            img.save(buffer, format=file_format)
            data = buffer.getvalue()
        filesize = len(data)
        size_deviation = filesize / max_file_size
        # print(f'size: {filesize}; factor: {size_deviation:.3f}')

        if size_deviation <= 1 + tolerance:
            # filesize fits
            image.media = data
            return image
        else:
            # filesize not good enough => adapt width and height
            # use sqrt of deviation since applied both in width and height
            new_width = img.size[0] / size_deviation ** 0.5
            new_height = new_width / aspect
            # resize from img_orig to not lose quality
            img = img_orig.resize((int(new_width), int(new_height)))
